package com.example;

import jakarta.enterprise.context.RequestScoped;
import jakarta.persistence.EntityManager;

@RequestScoped
public class MyService {
    EntityManager em;

    public MyService(){

    }
    public MyService(EntityManager em) {
        this.em = em;
    }

    public String te(){
        MyEntity e = new MyEntity();
        e.name="hello";
        e = em.merge(e);
        return em.find(MyEntity.class, e.id).name;
    }
}
