package com.example;

import jakarta.persistence.EntityManager;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


public class MyTest {

    MyService myService;

    @Test
    void testMy(){

        myService = new MyService(TestEntityManager.create());
        assertEquals("hello", myService.te());
    }

    @Test
    void createThree(){
        EntityManager em = TestEntityManager.create();
        myService = new MyService(em);
        em.getTransaction().begin();
        myService.te();
        myService.te();
        myService.te();
        em.getTransaction().commit();
        assertEquals(3, em.createQuery("from MyEntity e").getResultList().size());
    }
}
