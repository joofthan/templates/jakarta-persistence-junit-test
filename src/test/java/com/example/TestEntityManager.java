package com.example;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Persistence;

public class TestEntityManager {
    public static EntityManager create() {
        return Persistence.createEntityManagerFactory("IT").createEntityManager();
    }
}
